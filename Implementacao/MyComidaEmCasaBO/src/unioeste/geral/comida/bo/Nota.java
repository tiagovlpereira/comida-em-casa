package unioeste.geral.comida.bo;

import java.util.Date;

public class Nota {

	private ItemNota[] itensNota;
	private int idNota;
	private Date dataNota;
	private float totalNota;
	private float descontoTotal;
	private float valorLiquido;
	
	public ItemNota[] getItensNota() {
		return itensNota;
	}
	public void setItensNota(ItemNota[] itensNota) {
		this.itensNota = itensNota;
	}
	public int getIdNota() {
		return idNota;
	}
	public void setIdNota(int idNota) {
		this.idNota = idNota;
	}
	public Date getDataNota() {
		return dataNota;
	}
	public void setDataNota(Date dataNota) {
		this.dataNota = dataNota;
	}
	public float getTotalNota() {
		return totalNota;
	}
	public void setTotalNota(float totalNota) {
		this.totalNota = totalNota;
	}
	public float getDescontoTotal() {
		return descontoTotal;
	}
	public void setDescontoTotal(float descontoTotal) {
		this.descontoTotal = descontoTotal;
	}
	public float getValorLiquido() {
		return valorLiquido;
	}
	public void setValorLiquido(float valorLiquido) {
		this.valorLiquido = valorLiquido;
	}
	
	
	
}
