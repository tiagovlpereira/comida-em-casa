package unioeste.geral.comida.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import unioeste.apoio.BD.SQLConnector;
import unioeste.geral.comida.bo.Cardapio;
import unioeste.geral.comida.bo.Restaurante;

public class DAOCardapio {
	
	public ArrayList<Cardapio> obterCardapioPorRestaurante(Restaurante restaurante, SQLConnector connector) throws Exception{
		ArrayList<Cardapio> cardapios = new ArrayList<Cardapio>();
		
		String query = "SELECT * FROM cardapio WHERE idRestaurante = " + restaurante.getIdPessoa() + ";";
		ResultSet result = connector.executeQuery(query);
		
		while(result.next()) {		
			Cardapio cardapio = new Cardapio();
			
			cardapio.setDescricaoCardapio(result.getString("descricaoCardapio"));
			cardapio.setIdCardapio(result.getInt("idCardapio"));
			
			cardapios.add(cardapio);
		}
		
		return cardapios;
	}
	
}
