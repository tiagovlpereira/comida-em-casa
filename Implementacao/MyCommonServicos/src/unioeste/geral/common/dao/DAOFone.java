package unioeste.geral.common.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import unioeste.apoio.BD.SQLConnector;
import unioeste.geral.common.bo.Fone;
import unioeste.geral.common.bo.Pessoa;

public class DAOFone {
	
	public ArrayList<Fone> obterFoneCliente(Pessoa pessoa, SQLConnector connector) throws Exception{
		ArrayList<Fone> fone = new ArrayList<Fone>();
		
		String query = "SELECT * FROM fonecliente WHERE idCliente = " + pessoa.getIdPessoa() + ";";
		ResultSet result = connector.executeQuery(query);
		
		while (result.next()) {
			Fone f = new Fone();
			f.setFone(result.getString("numeroFone"), result.getInt("idFoneCliente"), result.getInt("idDDD"), result.getInt("idDDI"), result.getInt("idTipoFone"));
			
			fone.add(f);
		}
		
		return fone;
	}
	
	public ArrayList<Fone> obterFoneFuncionario(Pessoa pessoa, SQLConnector connector) throws Exception{
		ArrayList<Fone> fone = new ArrayList<Fone>();
		
		String query = "SELECT * FROM fonefuncionario WHERE idFuncionario = " + pessoa.getIdPessoa() + ";";
		ResultSet result = connector.executeQuery(query);
		
		while (result.next()) {
			Fone f = new Fone();
			f.setFone(result.getString("numeroFone"), result.getInt("idFoneFuncionario"), result.getInt("idDDD"), result.getInt("idDDI"), result.getInt("idTipoFone"));
			
			fone.add(f);
		}
		
		return fone;
	}
	
	public Fone inserirFoneCliente(Fone f, Pessoa pessoa, SQLConnector connector) throws Exception {
		String query = "INSERT INTO fonecliente (numeroFone, idCliente, idDDD, idDDI, idTipoFone) VALUES ('"+f.getNumeroTelefone()+"',"+pessoa.getIdPessoa()+","+f.getDdd().getIdDDD()+","+f.getDdi().getIdDDI()+","+f.getTipoFone().getIdTipoFone()+")";
		connector.executeUpdate(query);
		
		return f;
	}

	public ArrayList<Fone> obterFoneFornecedor(Pessoa p, SQLConnector connector) throws Exception{
		// TODO Auto-generated method stub
		ArrayList<Fone> fone = new ArrayList<Fone>();
		
		String query = "SELECT * FROM fonefornecedor WHERE idFornecedor = " + p.getIdPessoa() + ";";
		ResultSet result = connector.executeQuery(query);
		
		while (result.next()) {
			Fone f = new Fone();
			f.setFone(result.getString("numeroFone"), result.getInt("idFoneFornecedor"), result.getInt("idDDD"), result.getInt("idDDI"), result.getInt("idTipoFone"));
			
			fone.add(f);
		}
		
		return fone;
	}
	
	public ArrayList<Fone> obterFoneRestaurante(Pessoa p, SQLConnector connector) throws Exception{
		// TODO Auto-generated method stub
		ArrayList<Fone> fone = new ArrayList<Fone>();
		
		String query = "SELECT * FROM fonerestaurante WHERE idRestaurante = " + p.getIdPessoa() + ";";
		ResultSet result = connector.executeQuery(query);
		
		while (result.next()) {
			Fone f = new Fone();
			f.setFone(result.getString("numeroFone"), result.getInt("idFoneRestaurante"), result.getInt("idDDD"), result.getInt("idDDI"), result.getInt("idTipoFone"));
			
			fone.add(f);
		}
		
		return fone;
	}
	
}
