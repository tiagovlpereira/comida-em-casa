package unioeste.geral.comida.bo;

public class NotaVenda extends Nota{

	private Cliente cliente;
	private Entregador entregador;
	
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Entregador getEntregador() {
		return entregador;
	}
	public void setEntregador(Entregador entregador) {
		this.entregador = entregador;
	}
		
}
