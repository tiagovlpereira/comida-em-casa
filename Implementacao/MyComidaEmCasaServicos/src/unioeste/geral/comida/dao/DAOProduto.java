package unioeste.geral.comida.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import unioeste.apoio.BD.SQLConnector;
import unioeste.geral.comida.bo.Cardapio;
import unioeste.geral.comida.bo.Produto;
import unioeste.geral.comida.bo.Restaurante;

public class DAOProduto {
	
	public ArrayList<Produto> obterProdutoPorCardapio(Cardapio cardapio, SQLConnector connector) throws Exception{
		ArrayList<Produto> produtos = new ArrayList<Produto>();
		
		String query = "SELECT * FROM produto INNER JOIN cardapio_produto ON produto.idProduto = cardapio_produto.idProduto INNER JOIN cardapio ON cardapio.idCardapio = cardapio_produto.idCardapio WHERE cardapio.idCardapio ="+ cardapio.getIdCardapio() +";";
		ResultSet result = connector.executeQuery(query);
		
		while(result.next()) {		
			Produto produto = new Produto();
			
			produto.setPrecoCustoAtual(result.getFloat("produto.precoCustoAtual"));
			produto.setQuantidadeEstoque(result.getInt("produto.quantidadeEstoque"));
			produto.setNomeProduto(result.getString("produto.nomeProduto"));
			produto.setCodBarras(result.getString("produto.codBarras"));
			produto.setPrecoVendaAtual(result.getFloat("produto.precoVendaAtual"));
			Restaurante r = new Restaurante();
			r.setIdPessoa(result.getInt("produto.idRestaurante"));
			produto.setFornecedor(r);
			
			produtos.add(produto);
		}
		
		return produtos;
	}
	
}
