package unioeste.geral.endereco.tests;

import org.junit.Test;
import static org.junit.Assert.*;

import unioeste.geral.endereco.bo.Endereco;
import unioeste.geral.endereco.manager.UCEnderecoGeralServicos;

public class UCEnderecoGeralServicosTests {

	@Test
	public void obterEnderecoExterno_Aceitavel() throws Exception {
		UCEnderecoGeralServicos uc = new UCEnderecoGeralServicos();
		Endereco e = uc.obterEnderecoExterno("20020-050");
		
		assertEquals("Rio de Janeiro",e.getCidade().getNome());
		assertEquals("Avenida Churchill",e.getLogradouro().getNome());
		assertEquals("Centro",e.getBairro().getNome());
		
		
		
	}
	
}
