package unioeste.geral.comida.bo;

public class Produto {

	private int idProduto;
	private float precoCustoAtual;
	private int quantidadeEstoque;
	private String nomeProduto;
	private String codBarras;
	private float precoVendaAtual;
	private Categoria categoria;
	private Restaurante fornecedor;
	
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public Restaurante getFornecedor() {
		return fornecedor;
	}
	public void setFornecedor(Restaurante fornecedor) {
		this.fornecedor = fornecedor;
	}
	public int getIdProduto() {
		return idProduto;
	}
	public void setIdProduto(int idProduto) {
		this.idProduto = idProduto;
	}
	public float getPrecoCustoAtual() {
		return precoCustoAtual;
	}
	public void setPrecoCustoAtual(float precoCustoAtual) {
		this.precoCustoAtual = precoCustoAtual;
	}
	public int getQuantidadeEstoque() {
		return quantidadeEstoque;
	}
	public void setQuantidadeEstoque(int quantidadeEstoque) {
		this.quantidadeEstoque = quantidadeEstoque;
	}
	public String getNomeProduto() {
		return nomeProduto;
	}
	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}
	public String getCodBarras() {
		return codBarras;
	}
	public void setCodBarras(String codBarras) {
		this.codBarras = codBarras;
	}
	public float getPrecoVendaAtual() {
		return precoVendaAtual;
	}
	public void setPrecoVendaAtual(float precoVendaAtual) {
		this.precoVendaAtual = precoVendaAtual;
	}
	
	
	
}
