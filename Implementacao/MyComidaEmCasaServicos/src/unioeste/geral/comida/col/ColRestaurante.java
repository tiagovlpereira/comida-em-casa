package unioeste.geral.comida.col;

import java.util.ArrayList;

import unioeste.apoio.BD.SQLConnector;
import unioeste.geral.comida.bo.Restaurante;
import unioeste.geral.comida.dao.DAORestaurante;

public class ColRestaurante {

	public Restaurante obterRestaurantePorId(Restaurante restaurante, SQLConnector connector) throws Exception{
		DAORestaurante dao = new DAORestaurante();
		
		return dao.obterRestaurantePorId(restaurante, connector);
	}
	
	public ArrayList<Restaurante> obterTodosOsRestaurantes(SQLConnector connector) throws Exception{
		DAORestaurante dao = new DAORestaurante();
		
		return dao.obterTodosOsRestaurantes(connector);
	}
}
