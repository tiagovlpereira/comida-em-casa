package unioeste.geral.comida.manager;

import java.util.ArrayList;

import unioeste.apoio.BD.SQLConnector;
import unioeste.geral.comida.bo.Avaliacao;
import unioeste.geral.comida.bo.Cardapio;
import unioeste.geral.comida.bo.Produto;
import unioeste.geral.comida.bo.Restaurante;
import unioeste.geral.comida.col.ColAvaliacao;
import unioeste.geral.comida.col.ColCardapio;
import unioeste.geral.comida.col.ColProduto;
import unioeste.geral.comida.col.ColRestaurante;
import unioeste.geral.common.bo.Email;
import unioeste.geral.common.bo.EnderecoEspecifico;
import unioeste.geral.common.bo.Fone;
import unioeste.geral.common.col.ColDDD;
import unioeste.geral.common.col.ColDDI;
import unioeste.geral.common.col.ColEmail;
import unioeste.geral.common.col.ColFone;
import unioeste.geral.common.col.ColTipoFone;
import unioeste.geral.endereco.manager.UCEnderecoGeralServicos;

public class UCComidaEmCasaGeralServicos {

	public Restaurante obterRestaurantePorId(Restaurante r) throws Exception{
		ColRestaurante colRestaurante = new ColRestaurante();
		ColEmail colEmail = new ColEmail();
		ColFone colFone = new ColFone();
		ColDDD colDDD = new ColDDD();
		ColDDI colDDI = new ColDDI();
		ColTipoFone colTipoFone = new ColTipoFone();
		ColAvaliacao colAvaliacao = new ColAvaliacao();
		ColCardapio colCardapio = new ColCardapio();
		ColProduto colProduto = new ColProduto();
		SQLConnector connector = new SQLConnector();
		UCEnderecoGeralServicos ucEndereco = new UCEnderecoGeralServicos();
		
		r = colRestaurante.obterRestaurantePorId(r, connector);
		
		
		r.setEmail(colEmail.obterEmailRestaurante(r, connector).toArray(new Email[0]));
		
		ArrayList<Fone> fones = colFone.obterFoneRestaurante(r, connector);
		for(Fone f : fones) {
			f.setDdd(colDDD.obterDDDPorId(f.getDdd(), connector));
			f.setDdi(colDDI.obterDDIPorId(f.getDdi(), connector));
			f.setTipoFone(colTipoFone.obterTipoFonePorId(f.getTipoFone(), connector));
		}
		r.setFone(fones.toArray(new Fone[0]));
		
		EnderecoEspecifico ee = r.getEnderecoEspecifico();
		ee.setEndereco(ucEndereco.obterEnderecoPorId(r.getEnderecoEspecifico().getEndereco()));
		r.setEnderecoEspecifico(ee);
		
		r.setAvaliacoes(colAvaliacao.obterAvaliacoesPorRestaurante(r, connector).toArray(new Avaliacao[0]));
		r.calculaMediaAvaliacao();
		
		ArrayList<Cardapio> cardapios = colCardapio.obterCardapioPorRestaurante(r, connector);
		for(Cardapio c : cardapios){
			c.setProdutos(colProduto.obterProdutoPorCardapio(c, connector).toArray(new Produto[0]));
		}
		r.setCardapios(cardapios.toArray(new Cardapio[0]));
		
		
		connector.close();
		return r;
	}
	
	public ArrayList<Restaurante> obterTodosOsRestaurantes() throws Exception{
		ColRestaurante colRestaurante = new ColRestaurante();
		ColEmail colEmail = new ColEmail();
		ColFone colFone = new ColFone();
		ColDDD colDDD = new ColDDD();
		ColDDI colDDI = new ColDDI();
		ColTipoFone colTipoFone = new ColTipoFone();
		ColAvaliacao colAvaliacao = new ColAvaliacao();
		ColCardapio colCardapio = new ColCardapio();
		ColProduto colProduto = new ColProduto();
		SQLConnector connector = new SQLConnector();
		UCEnderecoGeralServicos ucEndereco = new UCEnderecoGeralServicos();
		ArrayList<Restaurante> restaurantes = colRestaurante.obterTodosOsRestaurantes(connector);
		
		
		for(Restaurante r : restaurantes) {
			r.setEmail(colEmail.obterEmailRestaurante(r, connector).toArray(new Email[0]));
			
			ArrayList<Fone> fones = colFone.obterFoneRestaurante(r, connector);
			for(Fone f : fones) {
				f.setDdd(colDDD.obterDDDPorId(f.getDdd(), connector));
				f.setDdi(colDDI.obterDDIPorId(f.getDdi(), connector));
				f.setTipoFone(colTipoFone.obterTipoFonePorId(f.getTipoFone(), connector));
			}
			r.setFone(fones.toArray(new Fone[0]));
			
			EnderecoEspecifico ee = r.getEnderecoEspecifico();
			ee.setEndereco(ucEndereco.obterEnderecoPorId(r.getEnderecoEspecifico().getEndereco()));
			r.setEnderecoEspecifico(ee);
			
			r.setAvaliacoes(colAvaliacao.obterAvaliacoesPorRestaurante(r, connector).toArray(new Avaliacao[0]));
			r.calculaMediaAvaliacao();
			
			ArrayList<Cardapio> cardapios = colCardapio.obterCardapioPorRestaurante(r, connector);
			for(Cardapio c : cardapios){
				c.setProdutos(colProduto.obterProdutoPorCardapio(c, connector).toArray(new Produto[0]));
			}
			r.setCardapios(cardapios.toArray(new Cardapio[0]));
			
		}
		
		
		connector.close();
		return restaurantes;
	}
	
}
