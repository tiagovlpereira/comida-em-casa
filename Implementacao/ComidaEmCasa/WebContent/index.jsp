<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="pt-BR">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Comida em casa</title>
	
	<script type="text/javascript" src="jquery-3.6.0.min.js" ></script>

	<script type="text/javascript">
		
		$(document).ready(function (){
			$("#botao").click();
		});

	</script>
	
	<style>
	
		#botao{
			display: none;
		}	
	
	</style>
	
</head>
<body>
	<form action="ConsultaRestaurantesServlet">
		<input id="botao" type="submit" value="Entrar">
		
	</form>
	
</body>
</html>