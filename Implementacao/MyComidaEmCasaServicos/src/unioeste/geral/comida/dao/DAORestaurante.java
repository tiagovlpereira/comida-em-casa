package unioeste.geral.comida.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import unioeste.apoio.BD.SQLConnector;
import unioeste.geral.comida.bo.Restaurante;
import unioeste.geral.common.bo.CNPJ;
import unioeste.geral.common.bo.EnderecoEspecifico;
import unioeste.geral.endereco.bo.Endereco;


public class DAORestaurante {

	public Restaurante obterRestaurantePorId(Restaurante restaurante, SQLConnector connector) throws Exception{
		String query = "SELECT * FROM restaurante WHERE idRestaurante = "+ restaurante.getIdPessoa()+";";
		ResultSet result = connector.executeQuery(query);
		result.next();
		
		restaurante.setNomeAbreviado(result.getString("nomeAbreviadoRestaurante"));
		restaurante.setNomeCompleto(result.getString("nomeCompletoRestaurante"));
		
		CNPJ cnpj = new CNPJ();
		cnpj.setNumeroDoc(result.getString("cnpj"));
		restaurante.setCnpj(cnpj);
		
		EnderecoEspecifico ee = new EnderecoEspecifico();
		ee.setNumero(result.getInt("numero"));
		ee.setComplemento(result.getString("complemento"));
		
		Endereco endereco = new Endereco();
		endereco.setIdEndereco(result.getInt("idEndereco"));
		ee.setEndereco(endereco);
		restaurante.setEnderecoEspecifico(ee);
		
		return restaurante;
	}
	
	public ArrayList<Restaurante> obterTodosOsRestaurantes(SQLConnector connector) throws Exception{
		ArrayList<Restaurante> restaurantes = new ArrayList<Restaurante>();
		
		String query = "SELECT * FROM restaurante;";
		ResultSet result = connector.executeQuery(query);
		
		while(result.next()) {
		
			Restaurante restaurante = new Restaurante();
			restaurante.setIdPessoa(result.getInt("idRestaurante"));
			restaurante.setNomeAbreviado(result.getString("nomeAbreviadoRestaurante"));
			restaurante.setNomeCompleto(result.getString("nomeCompletoRestaurante"));
			
			CNPJ cnpj = new CNPJ();
			cnpj.setNumeroDoc(result.getString("cnpj"));
			restaurante.setCnpj(cnpj);
			
			EnderecoEspecifico ee = new EnderecoEspecifico();
			ee.setNumero(result.getInt("numero"));
			ee.setComplemento(result.getString("complemento"));
			
			Endereco endereco = new Endereco();
			endereco.setIdEndereco(result.getInt("idEndereco"));
			ee.setEndereco(endereco);
			restaurante.setEnderecoEspecifico(ee);
			
			restaurantes.add(restaurante);
			
		}
		
		return restaurantes;
	}
	
}
