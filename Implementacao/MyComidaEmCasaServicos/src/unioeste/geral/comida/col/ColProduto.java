package unioeste.geral.comida.col;

import java.util.ArrayList;

import unioeste.apoio.BD.SQLConnector;
import unioeste.geral.comida.bo.Cardapio;
import unioeste.geral.comida.bo.Produto;
import unioeste.geral.comida.dao.DAOProduto;

public class ColProduto {

	public ArrayList<Produto> obterProdutoPorCardapio(Cardapio cardapio, SQLConnector connector) throws Exception{
		DAOProduto dao = new DAOProduto();
		
		return dao.obterProdutoPorCardapio(cardapio, connector);
	}
	
}
