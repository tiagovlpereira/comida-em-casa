package unioeste.geral.comida.bo;

import unioeste.geral.common.bo.PessoaJuridica;

public class Restaurante extends PessoaJuridica{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private float mediaAvaliacao;
	private Avaliacao[] avaliacoes;
	private Cardapio[] cardapios;
	
	public Cardapio[] getCardapios() {
		return cardapios;
	}

	public void setCardapios(Cardapio[] cardapios) {
		this.cardapios = cardapios;
	}

	public Cardapio getCardapio(int pos) {
		return cardapios[pos];
	}

	public void setCardapio(Cardapio cardapio, int pos) {
		this.cardapios[pos] = cardapio;
	}
	
	public Avaliacao[] getAvaliacoes() {
		return avaliacoes;
	}

	public void setAvaliacoes(Avaliacao[] avaliacoes) {
		this.avaliacoes = avaliacoes;
	}
	
	public Avaliacao getAvaliacaos(int pos) {
		return avaliacoes[pos];
	}

	public void setAvaliacao(Avaliacao avaliacao, int pos) {
		this.avaliacoes[pos] = avaliacao;
	}

	public float getMediaAvaliacao() {
		return mediaAvaliacao;
	}

	public void setMediaAvaliacao(float mediaAvaliacao) {
		this.mediaAvaliacao = mediaAvaliacao;
	}
	
	public void calculaMediaAvaliacao() {
		int count = 0;
		float somatoria = 0;
		for(Avaliacao a : this.avaliacoes) {
			count++;
			somatoria = somatoria + a.getNotaAvaliacao();
		}
		
		if(count > 0) this.mediaAvaliacao = somatoria/count;
		else this.mediaAvaliacao = 0;
	}
	
}
