package unioeste.geral.comida.dao;

import java.sql.ResultSet;
import java.util.ArrayList;

import unioeste.apoio.BD.SQLConnector;
import unioeste.geral.comida.bo.Avaliacao;
import unioeste.geral.comida.bo.Cliente;
import unioeste.geral.comida.bo.ClientePessoa;
import unioeste.geral.comida.bo.Restaurante;

public class DAOAvaliacao {

	public ArrayList<Avaliacao> obterAvaliacaoPorRestaurante(Restaurante restaurante, SQLConnector connector) throws Exception{
		ArrayList<Avaliacao> avaliacoes = new ArrayList<Avaliacao>();
		
		String query = "SELECT * FROM avaliacaorestaurante WHERE idRestaurante = " + restaurante.getIdPessoa() + ";";
		ResultSet result = connector.executeQuery(query);
		
		while(result.next()) {		
			Avaliacao avaliacao = new Avaliacao();
			
			Cliente cliente = new Cliente();
			ClientePessoa clientePessoa = new ClientePessoa();
			clientePessoa.setIdPessoa(result.getInt("idCliente"));
			cliente.setClientePessoa(clientePessoa);
			avaliacao.setCliente(cliente);
			
			avaliacao.setDescricaoAvaliacao(result.getString("descricaoAvaliacao"));
			avaliacao.setNotaAvaliacao(result.getInt("notaAvaliacao"));
			
			avaliacoes.add(avaliacao);
		}
		
		return avaliacoes;
	}
	
}
