package unioeste.geral.comida.bo;

public class Avaliacao {

	private int idAvaliacao;
	private int notaAvaliacao;
	private String descricaoAvaliacao;
	private Cliente cliente;
	
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public int getIdAvaliacao() {
		return idAvaliacao;
	}
	public void setIdAvaliacao(int idAvaliacao) {
		this.idAvaliacao = idAvaliacao;
	}
	public int getNotaAvaliacao() {
		return notaAvaliacao;
	}
	public void setNotaAvaliacao(int notaAaliacao) {
		this.notaAvaliacao = notaAaliacao;
	}
	public String getDescricaoAvaliacao() {
		return descricaoAvaliacao;
	}
	public void setDescricaoAvaliacao(String descricaoAvaliacao) {
		this.descricaoAvaliacao = descricaoAvaliacao;
	}
	
	
}
