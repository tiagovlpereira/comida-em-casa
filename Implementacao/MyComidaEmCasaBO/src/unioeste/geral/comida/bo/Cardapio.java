package unioeste.geral.comida.bo;

public class Cardapio {
	
	private int idCardapio;
	private String descricaoCardapio;
	private Produto[] produtos;
	
	public int getIdCardapio() {
		return idCardapio;
	}
	public void setIdCardapio(int idCardapio) {
		this.idCardapio = idCardapio;
	}
	public String getDescricaoCardapio() {
		return descricaoCardapio;
	}
	public void setDescricaoCardapio(String descricaoCardapio) {
		this.descricaoCardapio = descricaoCardapio;
	}
	public Produto[] getProdutos() {
		return produtos;
	}
	public void setProdutos(Produto[] produtos) {
		this.produtos = produtos;
	}
	
	public Produto getProduto(int pos) {
		return produtos[pos];
	}
	public void setProduto(Produto produto, int pos) {
		this.produtos[pos] = produto;
	}
	
		
}
