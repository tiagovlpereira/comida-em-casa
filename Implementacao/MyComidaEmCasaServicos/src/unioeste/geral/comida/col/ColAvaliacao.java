package unioeste.geral.comida.col;

import java.util.ArrayList;

import unioeste.apoio.BD.SQLConnector;
import unioeste.geral.comida.bo.Avaliacao;
import unioeste.geral.comida.bo.Restaurante;
import unioeste.geral.comida.dao.DAOAvaliacao;

public class ColAvaliacao {
	
	public ArrayList<Avaliacao> obterAvaliacoesPorRestaurante(Restaurante r, SQLConnector connector) throws Exception{
		DAOAvaliacao dao = new DAOAvaliacao();
		
		return dao.obterAvaliacaoPorRestaurante(r, connector);
	}
	
}
