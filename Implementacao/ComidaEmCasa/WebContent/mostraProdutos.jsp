<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<title>Comida em Casa</title>
		<meta charset="utf-8">
		

		<script type="text/javascript" src="jquery-3.6.0.min.js" ></script>


		<style>
			a:link, a:visited, a:active, a:hover  {
				text-decoration: none;
			}
			body{
				background-color: #CD5C5C;
				text-align: center;
			}
			h1{
				font-family: 'Averia Gruesa Libre', cursive;
				color: white;
				margin: auto;
				font-size: 100px;
				text-align: center;
			}
			div.restaurante{
				margin: auto;
				background-color: #FA8072;
				text-align: center;
				border-radius: 1em;
				padding: 50px;
				width: 20%;
				height: 20%;
				transition:all 0.9s;
				text-decoration: none;
				color: white;
				font-size: 200%;
				font-weight: bold;
				font-family: "Monaco", monospace;
			}
			div.restaurante:hover{
				background-color: white;
				color:#FA8072;
				width: 33%;
			}

			div.linha{
				display: flex;
				padding: 1%;
			}
			

			div#titulo{
				padding: 3%;
			}

			div#restaurantes{
				border-radius: 1em;
				margin: auto;
				background-color: #CD5C5C;
				width: 90%;
				height: 100%;
			}
			
			div#main{
				height: 100%;
			}

		</style>
	</head>
	
	<body>
		
		<div id="main">
			<div id="titulo">
				<h1>${r.nomeCompleto}</h1>
			</div>
			
			

			<div id="restaurantes">


				<div>${r.nomeCompleto}</div>


	

			</div>

		</div>

	</body>
	
</html>
