

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import unioeste.geral.endereco.bo.Endereco;
import unioeste.geral.endereco.manager.UCEnderecoGeralServicos;

/**
 * Servlet implementation class ConsultaEnderecoPorCepServlet
 */
@WebServlet("/endereco/ConsultaEnderecoPorCepServlet")
public class ConsultaEnderecoPorCepServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final Logger LOGGER = Logger.getLogger(ConsultaEnderecoPorCepServlet.class.getName());
       
	private static final UCEnderecoGeralServicos uc = new UCEnderecoGeralServicos();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultaEnderecoPorCepServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		try {
			Endereco e =  uc.obterEnderecoExterno(request.getParameter("cep"));
			
			PrintWriter out = response.getWriter();
			
			out.print(e.getLogradouro());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.log(null,"context",e);
		}
		
		
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		
		try {
			response.getWriter().append("Served at: ").append(request.getContextPath());
		}catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.log(null,"context",e);
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		
		try {
			doGet(request, response);
		}catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.log(null,"context",e);
		}
	}


}
