package unioeste.geral.comida.col;

import java.util.ArrayList;

import unioeste.apoio.BD.SQLConnector;
import unioeste.geral.comida.bo.Cardapio;
import unioeste.geral.comida.bo.Restaurante;
import unioeste.geral.comida.dao.DAOCardapio;

public class ColCardapio {

	public ArrayList<Cardapio> obterCardapioPorRestaurante(Restaurante r, SQLConnector connector) throws Exception{
		DAOCardapio dao = new DAOCardapio();
		
		return dao.obterCardapioPorRestaurante(r, connector);
	}
	
}
