

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import unioeste.geral.comida.bo.Produto;
import unioeste.geral.comida.bo.Restaurante;
import unioeste.geral.comida.manager.UCComidaEmCasaGeralServicos;

/**
 * Servlet implementation class ConsultaRestauranteServlet
 */
@WebServlet("/ConsultaRestauranteServlet")
public class ConsultaRestauranteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final Logger LOGGER = Logger.getLogger(ConsultaRestauranteServlet.class.getName());
	
	private static final UCComidaEmCasaGeralServicos uc = new UCComidaEmCasaGeralServicos();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConsultaRestauranteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {   	
    	Restaurante r = new Restaurante();
    	PrintWriter out = response.getWriter();
    	
    	
    	r.setIdPessoa(Integer.parseInt(request.getParameter("id")));
    	try {
			r = uc.obterRestaurantePorId(r);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.log(null,"context",e);
		}

    	out.print("<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"	<head>\r\n" + 
				"		<title>Comida em Casa</title>\r\n" + 
				"		<meta charset=\"utf-8\">\r\n" + 
				"		\r\n" + 
				"\r\n" + 
				"		<script type=\"text/javascript\" src=\"https://code.jquery.com/jquery-3.6.0.min.js\" ></script>\r\n" + 
				"\r\n" + 
				"		<script type=\"text/javascript\">\r\n" + 
				"			\r\n" + 
				"			$(document).ready(function (){\r\n" + 
				"\r\n" + 
				"				$( \".restaurante\" ).on( \"click\", function( event ) {\r\n" + 
				"				  console.log( $(this).text());\r\n" + 
				"				  console.log( $(this).find(\"input\").attr(\"value\"));\r\n" + 
				"				  var id = $(this).find(\"input\").attr(\"value\");\r\n" + 
				"\r\n" + 
				"				  $('<form action=\"ConsultaRestauranteServlet\"><input type=\"hidden\" name=\"id\" value=\"'+id+'\"></form>').appendTo('body').submit();\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"				  \r\n" + 
				"\r\n" + 
				"				});\r\n" + 
				"\r\n" + 
				"			});\r\n" + 
				"\r\n" + 
				"		</script>\r\n" + 
				"\r\n" + 
				"		<style>\r\n" + 
				"			a:link, a:visited, a:active, a:hover  {\r\n" + 
				"				text-decoration: none;\r\n" + 
				"			}\r\n" + 
				"			body{\r\n" + 
				"				background-color: #CD5C5C;\r\n" + 
				"				text-align: center;\r\n" + 
				"			}\r\n" + 
				"			h1{\r\n" + 
				"				font-family: 'Averia Gruesa Libre', cursive;\r\n" + 
				"				color: white;\r\n" + 
				"				margin: auto;\r\n" + 
				"				font-size: 100px;\r\n" + 
				"				text-align: center;\r\n" + 
				"			}\r\n" + 
				"			div.restaurante{\r\n" + 
				"				background-color: #FA8072;\r\n" + 
				"				text-align: center;\r\n" + 
				"				padding: 50px;\r\n" + 
				"				width: 33%;\r\n" + 
				"				height: 20%;\r\n" + 
				"				transition:all 0.9s;\r\n" + 
				"				text-decoration: none;\r\n" + 
				"				color: white;\r\n" + 
				"				font-size: 200%;\r\n" + 
				"				font-weight: bold;\r\n" + 
				"				font-family: \"Monaco\", monospace;\r\n" + 
				"			}\r\n" + 
				"			div.linha:hover{\r\n" + 
				"				background-color: white;\r\n" + 
				"				color:#FA8072;\r\n" + 
				"			}\r\n" + 
				"\r\n" + 
				"			div.linha{\r\n" + 
				"				display: flex;\r\n" + 
				"				padding: 1%;\r\n" + 
				"			}\r\n" + 
				"			\r\n" + 
				"\r\n" + 
				"			div#titulo{\r\n" + 
				"				padding: 3%;\r\n" + 
				"			}\r\n" + 
				"\r\n" + 
				"			div#restaurantes{\r\n" + 
				"				border-radius: 1em;\r\n" + 
				"				margin: auto;\r\n" + 
				"				background-color: #CD5C5C;\r\n" + 
				"				width: 90%;\r\n" + 
				"				height: 100%;\r\n" + 
				"			}\r\n" + 
				"			\r\n" + 
				"			div#main{\r\n" + 
				"				height: 100%;\r\n" + 
				"			}\r\n" + 
				"\r\n" + 
				"		</style>\r\n" + 
				"	</head>\r\n" + 
				"	\r\n" + 
				"	<body>\r\n" + 
				"		\r\n" + 
				"		<div id=\"main\">\r\n" + 
				"			<div id=\"titulo\">\r\n" + 
				"				<h1>" + r.getNomeCompleto() + "</h1>\r\n" + 
				"			</div>\r\n" + 
				"			\r\n" + 
				"			\r\n" + 
				"\r\n" + 
				"			<div id=\"restaurantes\">");
		
		for(Produto p : r.getCardapio(0).getProdutos()) {
			
			out.print("<div class=\"linha\">");
			out.print("<div class=\"restaurante\">" + p.getNomeProduto() + " <input type=hidden name=\"id\" value=\""+ p.getIdProduto() +"\"></div>");
			out.print("<div class=\"restaurante\">Preco: R$" + String.format("%.2f",p.getPrecoVendaAtual()) + "</div>");
			out.print("<div class=\"restaurante\">Disponivel: " + p.getQuantidadeEstoque() + "</div>");
			out.print("</div>");

		}
		
		
		out.print("			</div>\r\n" + 
				"\r\n" + 
				"		</div>\r\n" + 
				"\r\n" + 
				"	</body>\r\n" + 
				"	\r\n" + 
				"</html>\r\n" + 
				"");
		
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		
		try {
			response.getWriter().append("Served at: ").append(request.getContextPath());
		}catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.log(null,"context",e);
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		
		try {
			doGet(request, response);
		}catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.log(null,"context",e);
		}
	}

}
