CREATE TABLE AtividadeComercial (
 idAtividadeComercial INT NOT NULL,
 descricaoAtividadeComercial VARCHAR(100)
);

ALTER TABLE AtividadeComercial ADD CONSTRAINT PK_AtividadeComercial PRIMARY KEY (idAtividadeComercial);


CREATE TABLE Bairro (
 idBairro INT NOT NULL,
 nomeBairro VARCHAR(100) NOT NULL
);

ALTER TABLE Bairro ADD CONSTRAINT PK_Bairro PRIMARY KEY (idBairro);


CREATE TABLE DDD (
 idDDD INT NOT NULL,
 numeroDDD INT NOT NULL
);

ALTER TABLE DDD ADD CONSTRAINT PK_DDD PRIMARY KEY (idDDD);


CREATE TABLE DDI (
 idDDI INT NOT NULL,
 numeroDDI INT NOT NULL
);

ALTER TABLE DDI ADD CONSTRAINT PK_DDI PRIMARY KEY (idDDI);


CREATE TABLE Pais (
 idPais INT NOT NULL,
 nomePais VARCHAR(100) NOT NULL
);

ALTER TABLE Pais ADD CONSTRAINT PK_Pais PRIMARY KEY (idPais);


CREATE TABLE Sexo (
 idSexo INT NOT NULL,
 descricaoSexo VARCHAR(100) NOT NULL,
 siglaSexo VARCHAR(100) NOT NULL
);

ALTER TABLE Sexo ADD CONSTRAINT PK_Sexo PRIMARY KEY (idSexo);


CREATE TABLE TipoFone (
 idTipoFone INT NOT NULL,
 siglaTipoFone VARCHAR(100) NOT NULL,
 descricaoTipoFone VARCHAR(100)
);

ALTER TABLE TipoFone ADD CONSTRAINT PK_TipoFone PRIMARY KEY (idTipoFone);


CREATE TABLE TipoLogradouro (
 idTipoLogradouro INT NOT NULL,
 siglaTipo VARCHAR(100),
 nomeTIpo VARCHAR(100) NOT NULL
);

ALTER TABLE TipoLogradouro ADD CONSTRAINT PK_TipoLogradouro PRIMARY KEY (idTipoLogradouro);


CREATE TABLE UF (
 idUF INT NOT NULL,
 siglaUF VARCHAR(10),
 nomeUF VARCHAR(100) NOT NULL,
 idPais INT
);

ALTER TABLE UF ADD CONSTRAINT PK_UF PRIMARY KEY (idUF);


CREATE TABLE Cidade (
 idCidade INT NOT NULL,
 nomeCidade VARCHAR(100) NOT NULL,
 idUF INT
);

ALTER TABLE Cidade ADD CONSTRAINT PK_Cidade PRIMARY KEY (idCidade);


CREATE TABLE Logradouro (
 idLogradouro INT NOT NULL,
 nomeLogradouro VARCHAR(100) NOT NULL,
 idTipoLogradouro INT
);

ALTER TABLE Logradouro ADD CONSTRAINT PK_Logradouro PRIMARY KEY (idLogradouro);


CREATE TABLE OrgaoExpeditor (
 idOrgaoExpeditor INT NOT NULL,
 nomeOrgaoExpeditor VARCHAR(100) NOT NULL,
 idUF INT
);

ALTER TABLE OrgaoExpeditor ADD CONSTRAINT PK_OrgaoExpeditor PRIMARY KEY (idOrgaoExpeditor);


CREATE TABLE Endereco (
 idEndereco INT NOT NULL,
 cep VARCHAR(100) NOT NULL,
 idBairro INT,
 idCidade INT,
 idLogradouro INT
);

ALTER TABLE Endereco ADD CONSTRAINT PK_Endereco PRIMARY KEY (idEndereco);


CREATE TABLE Entregador (
 idEntregador INT NOT NULL,
 primeiroNomeEntregador VARCHAR(100),
 nomeMeioEntregador VARCHAR(100),
 ultimoNomeEntregador VARCHAR(100),
 nomeAbreviadoEntregador VARCHAR(100),
 nomeCompletoEntregador VARCHAR(100),
 cpf VARCHAR(100),
 idSexo INT,
 numeroDoc VARCHAR(100),
 dataExpedicao DATE,
 idOrgaoExpeditor INT
);

ALTER TABLE Entregador ADD CONSTRAINT PK_Entregador PRIMARY KEY (idEntregador);


CREATE TABLE Restaurante (
 idRestaurante INT NOT NULL,
 nomeAbreviadoRestaurante VARCHAR(100),
 nomeCompletoRestaurante VARCHAR(100),
 cnpj VARCHAR(100),
 numero INT,
 complemento VARCHAR(100),
 idEndereco INT
);

ALTER TABLE Restaurante ADD CONSTRAINT PK_Restaurante PRIMARY KEY (idRestaurante);


CREATE TABLE AtividadeComercial_Restaurante (
 idRestaurante INT NOT NULL,
 idAtividadeComercial INT NOT NULL
);

ALTER TABLE AtividadeComercial_Restaurante ADD CONSTRAINT PK_AtividadeComercial_Restaurante PRIMARY KEY (idRestaurante,idAtividadeComercial);


CREATE TABLE Cardapio (
 idCardapio INT NOT NULL,
 descriçãoCardapio VARCHAR(100),
 idRestaurante INT
);

ALTER TABLE Cardapio ADD CONSTRAINT PK_Cardapio PRIMARY KEY (idCardapio);


CREATE TABLE Cliente (
 idCliente INT NOT NULL,
 primeiroNomeCliente VARCHAR(100) NOT NULL,
 nomeMeioCliente VARCHAR(100),
 ultimoNomeCliente VARCHAR(100),
 nomeAbreviadoCliente VARCHAR(100),
 nomeCompletoCliente VARCHAR(100),
 cpf VARCHAR(100),
 idSexo INT,
 numero INT NOT NULL,
 complemento VARCHAR(100),
 idEndereco INT,
 numeroDoc VARCHAR(100),
 dataExpedicao DATE,
 idOrgaoExpeditor INT
);

ALTER TABLE Cliente ADD CONSTRAINT PK_Cliente PRIMARY KEY (idCliente);


CREATE TABLE ClienteEmpresa (
 idClienteEmpresa INT NOT NULL,
 primeiroNomeEmpresa VARCHAR(10) NOT NULL,
 nomeMeioEmpresa VARCHAR(10),
 ultimoNomeEmpresa VARCHAR(10),
 nomeAbreviadoEmpresa VARCHAR(10),
 cnpj VARCHAR(10) NOT NULL,
 numero INT NOT NULL,
 complemento VARCHAR(10),
 idEndereco INT,
 caminhoFoto VARCHAR(10),
 descricaoFoto VARCHAR(10)
);

ALTER TABLE ClienteEmpresa ADD CONSTRAINT PK_ClienteEmpresa PRIMARY KEY (idClienteEmpresa);


CREATE TABLE EmailCliente (
 idEmailCliente INT NOT NULL,
 descricaoEmail VARCHAR(100) NOT NULL,
 idCliente INT
);

ALTER TABLE EmailCliente ADD CONSTRAINT PK_EmailCliente PRIMARY KEY (idEmailCliente);


CREATE TABLE EmailRestaurante (
 idEmailRestaurante INT NOT NULL,
 descricaoEmail VARCHAR(100),
 idRestaurante INT
);

ALTER TABLE EmailRestaurante ADD CONSTRAINT PK_EmailRestaurante PRIMARY KEY (idEmailRestaurante);


CREATE TABLE FoneCliente (
 idFoneCliente INT NOT NULL,
 numeroFone VARCHAR(100) NOT NULL,
 idCliente INT,
 idDDI INT,
 idDDD INT,
 idTipoFone INT
);

ALTER TABLE FoneCliente ADD CONSTRAINT PK_FoneCliente PRIMARY KEY (idFoneCliente);


CREATE TABLE FoneRestaurante (
 idFoneRestaurante INT NOT NULL,
 numeroFone VARCHAR(100),
 idDDI INT,
 idDDD INT,
 idTipoFone INT,
 idRestaurante INT
);

ALTER TABLE FoneRestaurante ADD CONSTRAINT PK_FoneRestaurante PRIMARY KEY (idFoneRestaurante);


CREATE TABLE NotaCompra (
 idNotaCompra INT NOT NULL,
 dataNota DATE,
 totalNota FLOAT(10),
 descontoTotal FLOAT(10),
 valorLiquido FLOAT(10),
 idRestaurante INT
);

ALTER TABLE NotaCompra ADD CONSTRAINT PK_NotaCompra PRIMARY KEY (idNotaCompra);


CREATE TABLE NotaVenda (
 idNotaVenda INT NOT NULL,
 dataNota DATE,
 totalNota FLOAT(10),
 descontoTotal FLOAT(10),
 valorLiquido FLOAT(10),
 idCliente INT,
 idEntregador INT
);

ALTER TABLE NotaVenda ADD CONSTRAINT PK_NotaVenda PRIMARY KEY (idNotaVenda);


CREATE TABLE Produto (
 idProduto INT NOT NULL,
 precoCustoAtual FLOAT(10),
 quantidadeEstoque INT,
 nomeProduto VARCHAR(100),
 codBarras VARCHAR(100),
 precoVendaAtual FLOAT(10),
 idRestaurante INT
);

ALTER TABLE Produto ADD CONSTRAINT PK_Produto PRIMARY KEY (idProduto);


CREATE TABLE AtividadeComercial_Cliente (
 idAtividadeComercial INT NOT NULL,
 idCliente INT NOT NULL
);

ALTER TABLE AtividadeComercial_Cliente ADD CONSTRAINT PK_AtividadeComercial_Cliente PRIMARY KEY (idAtividadeComercial,idCliente);


CREATE TABLE AvaliacaoEntregador (
 idCliente INT NOT NULL,
 idEntregador INT NOT NULL,
 notaAvaliacao INT,
 descricaoAvaliacao VARCHAR(100)
);

ALTER TABLE AvaliacaoEntregador ADD CONSTRAINT PK_AvaliacaoEntregador PRIMARY KEY (idCliente,idEntregador);


CREATE TABLE AvaliacaoRestaurante (
 idRestaurante INT NOT NULL,
 idCliente INT NOT NULL,
 notaAvaliacao INT,
 descricaoAvaliacao VARCHAR(100)
);

ALTER TABLE AvaliacaoRestaurante ADD CONSTRAINT PK_AvaliacaoRestaurante PRIMARY KEY (idRestaurante,idCliente);


CREATE TABLE Cardapio_Produto (
 idCardapio INT NOT NULL,
 idProduto INT NOT NULL
);

ALTER TABLE Cardapio_Produto ADD CONSTRAINT PK_Cardapio_Produto PRIMARY KEY (idCardapio,idProduto);


CREATE TABLE ItemCompra (
 idNotaCompra INT NOT NULL,
 idProduto INT NOT NULL,
 quantidade INT,
 precoUnitario FLOAT(10)
);

ALTER TABLE ItemCompra ADD CONSTRAINT PK_ItemCompra PRIMARY KEY (idNotaCompra,idProduto);


CREATE TABLE ItemNota (
 idProduto INT NOT NULL,
 idNotaVenda INT NOT NULL,
 quantidade INT,
 precoUnitario FLOAT(10)
);

ALTER TABLE ItemNota ADD CONSTRAINT PK_ItemNota PRIMARY KEY (idProduto,idNotaVenda);


ALTER TABLE UF ADD CONSTRAINT FK_UF_0 FOREIGN KEY (idPais) REFERENCES Pais (idPais);


ALTER TABLE Cidade ADD CONSTRAINT FK_Cidade_0 FOREIGN KEY (idUF) REFERENCES UF (idUF);


ALTER TABLE Logradouro ADD CONSTRAINT FK_Logradouro_0 FOREIGN KEY (idTipoLogradouro) REFERENCES TipoLogradouro (idTipoLogradouro);


ALTER TABLE OrgaoExpeditor ADD CONSTRAINT FK_OrgaoExpeditor_0 FOREIGN KEY (idUF) REFERENCES UF (idUF);


ALTER TABLE Endereco ADD CONSTRAINT FK_Endereco_0 FOREIGN KEY (idBairro) REFERENCES Bairro (idBairro);
ALTER TABLE Endereco ADD CONSTRAINT FK_Endereco_1 FOREIGN KEY (idCidade) REFERENCES Cidade (idCidade);
ALTER TABLE Endereco ADD CONSTRAINT FK_Endereco_2 FOREIGN KEY (idLogradouro) REFERENCES Logradouro (idLogradouro);


ALTER TABLE Entregador ADD CONSTRAINT FK_Entregador_0 FOREIGN KEY (idSexo) REFERENCES Sexo (idSexo);
ALTER TABLE Entregador ADD CONSTRAINT FK_Entregador_1 FOREIGN KEY (idOrgaoExpeditor) REFERENCES OrgaoExpeditor (idOrgaoExpeditor);


ALTER TABLE Restaurante ADD CONSTRAINT FK_Restaurante_0 FOREIGN KEY (idEndereco) REFERENCES Endereco (idEndereco);


ALTER TABLE AtividadeComercial_Restaurante ADD CONSTRAINT FK_AtividadeComercial_Restaurante_0 FOREIGN KEY (idRestaurante) REFERENCES Restaurante (idRestaurante);
ALTER TABLE AtividadeComercial_Restaurante ADD CONSTRAINT FK_AtividadeComercial_Restaurante_1 FOREIGN KEY (idAtividadeComercial) REFERENCES AtividadeComercial (idAtividadeComercial);


ALTER TABLE Cardapio ADD CONSTRAINT FK_Cardapio_0 FOREIGN KEY (idRestaurante) REFERENCES Restaurante (idRestaurante);


ALTER TABLE Cliente ADD CONSTRAINT FK_Cliente_0 FOREIGN KEY (idSexo) REFERENCES Sexo (idSexo);
ALTER TABLE Cliente ADD CONSTRAINT FK_Cliente_1 FOREIGN KEY (idEndereco) REFERENCES Endereco (idEndereco);
ALTER TABLE Cliente ADD CONSTRAINT FK_Cliente_2 FOREIGN KEY (idOrgaoExpeditor) REFERENCES OrgaoExpeditor (idOrgaoExpeditor);


ALTER TABLE ClienteEmpresa ADD CONSTRAINT FK_ClienteEmpresa_0 FOREIGN KEY (idEndereco) REFERENCES Endereco (idEndereco);


ALTER TABLE EmailCliente ADD CONSTRAINT FK_EmailCliente_0 FOREIGN KEY (idCliente) REFERENCES Cliente (idCliente);


ALTER TABLE EmailRestaurante ADD CONSTRAINT FK_EmailRestaurante_0 FOREIGN KEY (idRestaurante) REFERENCES Restaurante (idRestaurante);


ALTER TABLE FoneCliente ADD CONSTRAINT FK_FoneCliente_0 FOREIGN KEY (idCliente) REFERENCES Cliente (idCliente);
ALTER TABLE FoneCliente ADD CONSTRAINT FK_FoneCliente_1 FOREIGN KEY (idDDI) REFERENCES DDI (idDDI);
ALTER TABLE FoneCliente ADD CONSTRAINT FK_FoneCliente_2 FOREIGN KEY (idDDD) REFERENCES DDD (idDDD);
ALTER TABLE FoneCliente ADD CONSTRAINT FK_FoneCliente_3 FOREIGN KEY (idTipoFone) REFERENCES TipoFone (idTipoFone);


ALTER TABLE FoneRestaurante ADD CONSTRAINT FK_FoneRestaurante_0 FOREIGN KEY (idDDI) REFERENCES DDI (idDDI);
ALTER TABLE FoneRestaurante ADD CONSTRAINT FK_FoneRestaurante_1 FOREIGN KEY (idDDD) REFERENCES DDD (idDDD);
ALTER TABLE FoneRestaurante ADD CONSTRAINT FK_FoneRestaurante_2 FOREIGN KEY (idTipoFone) REFERENCES TipoFone (idTipoFone);
ALTER TABLE FoneRestaurante ADD CONSTRAINT FK_FoneRestaurante_3 FOREIGN KEY (idRestaurante) REFERENCES Restaurante (idRestaurante);


ALTER TABLE NotaCompra ADD CONSTRAINT FK_NotaCompra_0 FOREIGN KEY (idRestaurante) REFERENCES Restaurante (idRestaurante);


ALTER TABLE NotaVenda ADD CONSTRAINT FK_NotaVenda_0 FOREIGN KEY (idCliente) REFERENCES Cliente (idCliente);
ALTER TABLE NotaVenda ADD CONSTRAINT FK_NotaVenda_1 FOREIGN KEY (idEntregador) REFERENCES Entregador (idEntregador);


ALTER TABLE Produto ADD CONSTRAINT FK_Produto_0 FOREIGN KEY (idRestaurante) REFERENCES Restaurante (idRestaurante);


ALTER TABLE AtividadeComercial_Cliente ADD CONSTRAINT FK_AtividadeComercial_Cliente_0 FOREIGN KEY (idAtividadeComercial) REFERENCES AtividadeComercial (idAtividadeComercial);
ALTER TABLE AtividadeComercial_Cliente ADD CONSTRAINT FK_AtividadeComercial_Cliente_1 FOREIGN KEY (idCliente) REFERENCES Cliente (idCliente);


ALTER TABLE AvaliacaoEntregador ADD CONSTRAINT FK_AvaliacaoEntregador_0 FOREIGN KEY (idCliente) REFERENCES Cliente (idCliente);
ALTER TABLE AvaliacaoEntregador ADD CONSTRAINT FK_AvaliacaoEntregador_1 FOREIGN KEY (idEntregador) REFERENCES Entregador (idEntregador);


ALTER TABLE AvaliacaoRestaurante ADD CONSTRAINT FK_AvaliacaoRestaurante_0 FOREIGN KEY (idRestaurante) REFERENCES Restaurante (idRestaurante);
ALTER TABLE AvaliacaoRestaurante ADD CONSTRAINT FK_AvaliacaoRestaurante_1 FOREIGN KEY (idCliente) REFERENCES Cliente (idCliente);


ALTER TABLE Cardapio_Produto ADD CONSTRAINT FK_Cardapio_Produto_0 FOREIGN KEY (idCardapio) REFERENCES Cardapio (idCardapio);
ALTER TABLE Cardapio_Produto ADD CONSTRAINT FK_Cardapio_Produto_1 FOREIGN KEY (idProduto) REFERENCES Produto (idProduto);


ALTER TABLE ItemCompra ADD CONSTRAINT FK_ItemCompra_0 FOREIGN KEY (idNotaCompra) REFERENCES NotaCompra (idNotaCompra);
ALTER TABLE ItemCompra ADD CONSTRAINT FK_ItemCompra_1 FOREIGN KEY (idProduto) REFERENCES Produto (idProduto);


ALTER TABLE ItemNota ADD CONSTRAINT FK_ItemNota_0 FOREIGN KEY (idProduto) REFERENCES Produto (idProduto);
ALTER TABLE ItemNota ADD CONSTRAINT FK_ItemNota_1 FOREIGN KEY (idNotaVenda) REFERENCES NotaVenda (idNotaVenda);


