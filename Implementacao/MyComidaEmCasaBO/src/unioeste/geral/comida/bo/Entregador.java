package unioeste.geral.comida.bo;

import unioeste.geral.common.bo.PessoaFisica;

public class Entregador extends PessoaFisica{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private float mediaAvaliacao;
	private Avaliacao[] avaliacoes;

	public Avaliacao[] getAvaliacoes() {
		return avaliacoes;
	}

	public void setAvaliacoes(Avaliacao[] avaliacoes) {
		this.avaliacoes = avaliacoes;
	}
	
	public Avaliacao getAvaliacaos(int pos) {
		return avaliacoes[pos];
	}

	public void setAvaliacao(Avaliacao avaliacao, int pos) {
		this.avaliacoes[pos] = avaliacao;
	}
	
	public float getMediaAvaliacao() {
		return mediaAvaliacao;
	}

	public void setMediaAvaliacao(float mediaAvaliacao) {
		this.mediaAvaliacao = mediaAvaliacao;
	}

}
