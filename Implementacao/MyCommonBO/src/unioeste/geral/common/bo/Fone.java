package unioeste.geral.common.bo;

import java.io.Serializable;

public class Fone implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int idFone;

	private String numeroTelefone;

	private DDD ddd;

	private DDI ddi;

	private TipoFone tipoFone;

	public int getIdFone() {
		return idFone;
	}

	public void setIdFone(int idFone) {
		this.idFone = idFone;
	}

	public String getNumeroTelefone() {
		return numeroTelefone;
	}

	public void setNumeroTelefone(String numeroTelefone) {
		this.numeroTelefone = numeroTelefone;
	}

	public DDD getDdd() {
		return ddd;
	}

	public void setDdd(DDD ddd) {
		this.ddd = ddd;
	}

	public DDI getDdi() {
		return ddi;
	}

	public void setDdi(DDI ddi) {
		this.ddi = ddi;
	}

	public TipoFone getTipoFone() {
		return tipoFone;
	}

	public void setTipoFone(TipoFone tipoFone) {
		this.tipoFone = tipoFone;
	}
	
	public Fone setFone(String numeroFone, int idFone, int idDDD, int idDDI, int idTipoFone) {
		Fone f = new Fone();
		
		f.setNumeroTelefone(numeroFone);
		f.setIdFone(idFone);
		
		DDD dddInternal = new DDD();
		DDI ddiInternal = new DDI();
		TipoFone tf = new TipoFone();
		
		dddInternal.setIdDDD(idDDD);
		ddiInternal.setIdDDI(idDDI);
		tf.setIdTipoFone(idTipoFone);
		
		f.setDdd(ddd);
		f.setDdi(ddi);
		f.setTipoFone(tf);
		
		return f;
	}

}
