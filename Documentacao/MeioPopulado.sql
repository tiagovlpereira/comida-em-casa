-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `atividadecomercial`
--

DROP TABLE IF EXISTS `atividadecomercial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `atividadecomercial` (
  `idAtividadeComercial` int(11) NOT NULL,
  `descricaoAtividadeComercial` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idAtividadeComercial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atividadecomercial`
--

LOCK TABLES `atividadecomercial` WRITE;
/*!40000 ALTER TABLE `atividadecomercial` DISABLE KEYS */;
INSERT INTO `atividadecomercial` VALUES (1,'Comercio'),(2,'Suporte');
/*!40000 ALTER TABLE `atividadecomercial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atividadecomercial_cliente`
--

DROP TABLE IF EXISTS `atividadecomercial_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `atividadecomercial_cliente` (
  `idAtividadeComercial` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  PRIMARY KEY (`idAtividadeComercial`,`idCliente`),
  KEY `FK_AtividadeComercial_Cliente_1` (`idCliente`),
  CONSTRAINT `FK_AtividadeComercial_Cliente_0` FOREIGN KEY (`idAtividadeComercial`) REFERENCES `atividadecomercial` (`idAtividadeComercial`),
  CONSTRAINT `FK_AtividadeComercial_Cliente_1` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atividadecomercial_cliente`
--

LOCK TABLES `atividadecomercial_cliente` WRITE;
/*!40000 ALTER TABLE `atividadecomercial_cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `atividadecomercial_cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atividadecomercial_restaurante`
--

DROP TABLE IF EXISTS `atividadecomercial_restaurante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `atividadecomercial_restaurante` (
  `idRestaurante` int(11) NOT NULL,
  `idAtividadeComercial` int(11) NOT NULL,
  PRIMARY KEY (`idRestaurante`,`idAtividadeComercial`),
  KEY `FK_AtividadeComercial_Restaurante_1` (`idAtividadeComercial`),
  CONSTRAINT `FK_AtividadeComercial_Restaurante_0` FOREIGN KEY (`idRestaurante`) REFERENCES `restaurante` (`idRestaurante`),
  CONSTRAINT `FK_AtividadeComercial_Restaurante_1` FOREIGN KEY (`idAtividadeComercial`) REFERENCES `atividadecomercial` (`idAtividadeComercial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atividadecomercial_restaurante`
--

LOCK TABLES `atividadecomercial_restaurante` WRITE;
/*!40000 ALTER TABLE `atividadecomercial_restaurante` DISABLE KEYS */;
/*!40000 ALTER TABLE `atividadecomercial_restaurante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avaliacaoentregador`
--

DROP TABLE IF EXISTS `avaliacaoentregador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `avaliacaoentregador` (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `idEntregador` int(11) NOT NULL,
  `notaAvaliacao` int(11) DEFAULT NULL,
  `descricaoAvaliacao` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idCliente`,`idEntregador`),
  KEY `FK_AvaliacaoEntregador_1` (`idEntregador`),
  CONSTRAINT `FK_AvaliacaoEntregador_0` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`),
  CONSTRAINT `FK_AvaliacaoEntregador_1` FOREIGN KEY (`idEntregador`) REFERENCES `entregador` (`idEntregador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avaliacaoentregador`
--

LOCK TABLES `avaliacaoentregador` WRITE;
/*!40000 ALTER TABLE `avaliacaoentregador` DISABLE KEYS */;
/*!40000 ALTER TABLE `avaliacaoentregador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `avaliacaorestaurante`
--

DROP TABLE IF EXISTS `avaliacaorestaurante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `avaliacaorestaurante` (
  `idRestaurante` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `notaAvaliacao` int(11) DEFAULT NULL,
  `descricaoAvaliacao` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idRestaurante`,`idCliente`),
  KEY `FK_AvaliacaoRestaurante_1` (`idCliente`),
  CONSTRAINT `FK_AvaliacaoRestaurante_0` FOREIGN KEY (`idRestaurante`) REFERENCES `restaurante` (`idRestaurante`),
  CONSTRAINT `FK_AvaliacaoRestaurante_1` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `avaliacaorestaurante`
--

LOCK TABLES `avaliacaorestaurante` WRITE;
/*!40000 ALTER TABLE `avaliacaorestaurante` DISABLE KEYS */;
/*!40000 ALTER TABLE `avaliacaorestaurante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bairro`
--

DROP TABLE IF EXISTS `bairro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bairro` (
  `idBairro` int(11) NOT NULL,
  `nomeBairro` varchar(100) NOT NULL,
  PRIMARY KEY (`idBairro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bairro`
--

LOCK TABLES `bairro` WRITE;
/*!40000 ALTER TABLE `bairro` DISABLE KEYS */;
INSERT INTO `bairro` VALUES (1,'Centro'),(2,'Vila A'),(3,'Vila B'),(4,'Vila C'),(5,'Parque Presidente');
/*!40000 ALTER TABLE `bairro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cardapio`
--

DROP TABLE IF EXISTS `cardapio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cardapio` (
  `idCardapio` int(11) NOT NULL,
  `descricaoCardapio` varchar(100) DEFAULT NULL,
  `idRestaurante` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCardapio`),
  KEY `FK_Cardapio_0` (`idRestaurante`),
  CONSTRAINT `FK_Cardapio_0` FOREIGN KEY (`idRestaurante`) REFERENCES `restaurante` (`idRestaurante`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cardapio`
--

LOCK TABLES `cardapio` WRITE;
/*!40000 ALTER TABLE `cardapio` DISABLE KEYS */;
INSERT INTO `cardapio` VALUES (1,'default',1),(2,'default',2),(3,'default',3),(4,'default',4),(5,'default',5),(6,'default',6),(7,'default',7),(8,'default',8),(9,'default',9);
/*!40000 ALTER TABLE `cardapio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cardapio_produto`
--

DROP TABLE IF EXISTS `cardapio_produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cardapio_produto` (
  `idCardapio` int(11) NOT NULL,
  `idProduto` int(11) NOT NULL,
  PRIMARY KEY (`idCardapio`,`idProduto`),
  KEY `FK_Cardapio_Produto_1` (`idProduto`),
  CONSTRAINT `FK_Cardapio_Produto_0` FOREIGN KEY (`idCardapio`) REFERENCES `cardapio` (`idCardapio`),
  CONSTRAINT `FK_Cardapio_Produto_1` FOREIGN KEY (`idProduto`) REFERENCES `produto` (`idProduto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cardapio_produto`
--

LOCK TABLES `cardapio_produto` WRITE;
/*!40000 ALTER TABLE `cardapio_produto` DISABLE KEYS */;
INSERT INTO `cardapio_produto` VALUES (1,1),(1,2),(1,3),(2,4),(2,5),(2,6),(3,7),(3,8),(3,9),(4,10),(4,11),(4,12),(5,13),(5,14),(5,15),(6,16),(6,17),(6,18),(7,19),(7,20),(7,21),(8,22),(8,23),(8,24),(9,25),(9,26),(9,27);
/*!40000 ALTER TABLE `cardapio_produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cidade`
--

DROP TABLE IF EXISTS `cidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cidade` (
  `idCidade` int(11) NOT NULL,
  `nomeCidade` varchar(100) NOT NULL,
  `idUF` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCidade`),
  KEY `FK_Cidade_0` (`idUF`),
  CONSTRAINT `FK_Cidade_0` FOREIGN KEY (`idUF`) REFERENCES `uf` (`idUF`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cidade`
--

LOCK TABLES `cidade` WRITE;
/*!40000 ALTER TABLE `cidade` DISABLE KEYS */;
INSERT INTO `cidade` VALUES (1,'Foz do Iguacu',1),(2,'Londrina',1),(3,'Medianeira',1),(4,'Curitiba',1);
/*!40000 ALTER TABLE `cidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL,
  `primeiroNomeCliente` varchar(100) NOT NULL,
  `nomeMeioCliente` varchar(100) DEFAULT NULL,
  `ultimoNomeCliente` varchar(100) DEFAULT NULL,
  `nomeAbreviadoCliente` varchar(100) DEFAULT NULL,
  `nomeCompletoCliente` varchar(100) DEFAULT NULL,
  `cpf` varchar(100) DEFAULT NULL,
  `idSexo` int(11) DEFAULT NULL,
  `numero` int(11) NOT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `idEndereco` int(11) DEFAULT NULL,
  `numeroDoc` varchar(100) DEFAULT NULL,
  `dataExpedicao` date DEFAULT NULL,
  `idOrgaoExpeditor` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCliente`),
  KEY `FK_Cliente_0` (`idSexo`),
  KEY `FK_Cliente_1` (`idEndereco`),
  KEY `FK_Cliente_2` (`idOrgaoExpeditor`),
  CONSTRAINT `FK_Cliente_0` FOREIGN KEY (`idSexo`) REFERENCES `sexo` (`idSexo`),
  CONSTRAINT `FK_Cliente_1` FOREIGN KEY (`idEndereco`) REFERENCES `endereco` (`idEndereco`),
  CONSTRAINT `FK_Cliente_2` FOREIGN KEY (`idOrgaoExpeditor`) REFERENCES `orgaoexpeditor` (`idOrgaoExpeditor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clienteempresa`
--

DROP TABLE IF EXISTS `clienteempresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clienteempresa` (
  `idClienteEmpresa` int(11) NOT NULL AUTO_INCREMENT,
  `primeiroNomeEmpresa` varchar(20) NOT NULL,
  `nomeMeioEmpresa` varchar(20) DEFAULT NULL,
  `ultimoNomeEmpresa` varchar(20) DEFAULT NULL,
  `nomeAbreviadoEmpresa` varchar(20) DEFAULT NULL,
  `cnpj` varchar(20) NOT NULL,
  `numero` int(11) NOT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `idEndereco` int(11) DEFAULT NULL,
  `caminhoFoto` varchar(100) DEFAULT NULL,
  `descricaoFoto` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idClienteEmpresa`),
  KEY `FK_ClienteEmpresa_0` (`idEndereco`),
  CONSTRAINT `FK_ClienteEmpresa_0` FOREIGN KEY (`idEndereco`) REFERENCES `endereco` (`idEndereco`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clienteempresa`
--

LOCK TABLES `clienteempresa` WRITE;
/*!40000 ALTER TABLE `clienteempresa` DISABLE KEYS */;
/*!40000 ALTER TABLE `clienteempresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ddd`
--

DROP TABLE IF EXISTS `ddd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ddd` (
  `idDDD` int(11) NOT NULL,
  `numeroDDD` int(11) NOT NULL,
  PRIMARY KEY (`idDDD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ddd`
--

LOCK TABLES `ddd` WRITE;
/*!40000 ALTER TABLE `ddd` DISABLE KEYS */;
INSERT INTO `ddd` VALUES (1,45),(3,57);
/*!40000 ALTER TABLE `ddd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ddi`
--

DROP TABLE IF EXISTS `ddi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ddi` (
  `idDDI` int(11) NOT NULL,
  `numeroDDI` int(11) NOT NULL,
  PRIMARY KEY (`idDDI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ddi`
--

LOCK TABLES `ddi` WRITE;
/*!40000 ALTER TABLE `ddi` DISABLE KEYS */;
INSERT INTO `ddi` VALUES (1,55),(2,1),(3,353);
/*!40000 ALTER TABLE `ddi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emailcliente`
--

DROP TABLE IF EXISTS `emailcliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `emailcliente` (
  `idEmailCliente` int(11) NOT NULL,
  `descricaoEmail` varchar(100) NOT NULL,
  `idCliente` int(11) DEFAULT NULL,
  PRIMARY KEY (`idEmailCliente`),
  KEY `FK_EmailCliente_0` (`idCliente`),
  CONSTRAINT `FK_EmailCliente_0` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailcliente`
--

LOCK TABLES `emailcliente` WRITE;
/*!40000 ALTER TABLE `emailcliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `emailcliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emailrestaurante`
--

DROP TABLE IF EXISTS `emailrestaurante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `emailrestaurante` (
  `idEmailRestaurante` int(11) NOT NULL,
  `descricaoEmail` varchar(100) DEFAULT NULL,
  `idRestaurante` int(11) DEFAULT NULL,
  PRIMARY KEY (`idEmailRestaurante`),
  KEY `FK_EmailRestaurante_0` (`idRestaurante`),
  CONSTRAINT `FK_EmailRestaurante_0` FOREIGN KEY (`idRestaurante`) REFERENCES `restaurante` (`idRestaurante`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailrestaurante`
--

LOCK TABLES `emailrestaurante` WRITE;
/*!40000 ALTER TABLE `emailrestaurante` DISABLE KEYS */;
INSERT INTO `emailrestaurante` VALUES (1,'doceamor@gmail.com',1),(2,'subway@gmail.com',2),(3,'ponto_mineiro@gmail.com',3),(4,'bobs@gmail.com',4),(5,'burguer_king@gmail.com',5),(6,'cacau_show@gmail.com',6),(7,'giraffas@gmail.com',7),(8,'mcdonalds@gmail.com',8),(9,'madero@gmail.com',9);
/*!40000 ALTER TABLE `emailrestaurante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `endereco`
--

DROP TABLE IF EXISTS `endereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `endereco` (
  `idEndereco` int(11) NOT NULL,
  `cep` varchar(100) NOT NULL,
  `idBairro` int(11) DEFAULT NULL,
  `idCidade` int(11) DEFAULT NULL,
  `idLogradouro` int(11) DEFAULT NULL,
  PRIMARY KEY (`idEndereco`),
  KEY `FK_Endereco_0` (`idBairro`),
  KEY `FK_Endereco_1` (`idCidade`),
  KEY `FK_Endereco_2` (`idLogradouro`),
  CONSTRAINT `FK_Endereco_0` FOREIGN KEY (`idBairro`) REFERENCES `bairro` (`idBairro`),
  CONSTRAINT `FK_Endereco_1` FOREIGN KEY (`idCidade`) REFERENCES `cidade` (`idCidade`),
  CONSTRAINT `FK_Endereco_2` FOREIGN KEY (`idLogradouro`) REFERENCES `logradouro` (`idLogradouro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `endereco`
--

LOCK TABLES `endereco` WRITE;
/*!40000 ALTER TABLE `endereco` DISABLE KEYS */;
INSERT INTO `endereco` VALUES (1,'85867267',2,1,1),(2,'85863000',5,1,3);
/*!40000 ALTER TABLE `endereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entregador`
--

DROP TABLE IF EXISTS `entregador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `entregador` (
  `idEntregador` int(11) NOT NULL,
  `primeiroNomeEntregador` varchar(100) DEFAULT NULL,
  `nomeMeioEntregador` varchar(100) DEFAULT NULL,
  `ultimoNomeEntregador` varchar(100) DEFAULT NULL,
  `nomeAbreviadoEntregador` varchar(100) DEFAULT NULL,
  `nomeCompletoEntregador` varchar(100) DEFAULT NULL,
  `cpf` varchar(100) DEFAULT NULL,
  `idSexo` int(11) DEFAULT NULL,
  `numeroDoc` varchar(100) DEFAULT NULL,
  `dataExpedicao` date DEFAULT NULL,
  `idOrgaoExpeditor` int(11) DEFAULT NULL,
  PRIMARY KEY (`idEntregador`),
  KEY `FK_Entregador_0` (`idSexo`),
  KEY `FK_Entregador_1` (`idOrgaoExpeditor`),
  CONSTRAINT `FK_Entregador_0` FOREIGN KEY (`idSexo`) REFERENCES `sexo` (`idSexo`),
  CONSTRAINT `FK_Entregador_1` FOREIGN KEY (`idOrgaoExpeditor`) REFERENCES `orgaoexpeditor` (`idOrgaoExpeditor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entregador`
--

LOCK TABLES `entregador` WRITE;
/*!40000 ALTER TABLE `entregador` DISABLE KEYS */;
/*!40000 ALTER TABLE `entregador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fonecliente`
--

DROP TABLE IF EXISTS `fonecliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fonecliente` (
  `idFoneCliente` int(11) NOT NULL,
  `numeroFone` varchar(100) NOT NULL,
  `idCliente` int(11) DEFAULT NULL,
  `idDDI` int(11) DEFAULT NULL,
  `idDDD` int(11) DEFAULT NULL,
  `idTipoFone` int(11) DEFAULT NULL,
  PRIMARY KEY (`idFoneCliente`),
  KEY `FK_FoneCliente_0` (`idCliente`),
  KEY `FK_FoneCliente_1` (`idDDI`),
  KEY `FK_FoneCliente_2` (`idDDD`),
  KEY `FK_FoneCliente_3` (`idTipoFone`),
  CONSTRAINT `FK_FoneCliente_0` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`),
  CONSTRAINT `FK_FoneCliente_1` FOREIGN KEY (`idDDI`) REFERENCES `ddi` (`idDDI`),
  CONSTRAINT `FK_FoneCliente_2` FOREIGN KEY (`idDDD`) REFERENCES `ddd` (`idDDD`),
  CONSTRAINT `FK_FoneCliente_3` FOREIGN KEY (`idTipoFone`) REFERENCES `tipofone` (`idTipoFone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fonecliente`
--

LOCK TABLES `fonecliente` WRITE;
/*!40000 ALTER TABLE `fonecliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `fonecliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fonerestaurante`
--

DROP TABLE IF EXISTS `fonerestaurante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fonerestaurante` (
  `idFoneRestaurante` int(11) NOT NULL,
  `numeroFone` varchar(100) DEFAULT NULL,
  `idDDI` int(11) DEFAULT NULL,
  `idDDD` int(11) DEFAULT NULL,
  `idTipoFone` int(11) DEFAULT NULL,
  `idRestaurante` int(11) DEFAULT NULL,
  PRIMARY KEY (`idFoneRestaurante`),
  KEY `FK_FoneRestaurante_0` (`idDDI`),
  KEY `FK_FoneRestaurante_1` (`idDDD`),
  KEY `FK_FoneRestaurante_2` (`idTipoFone`),
  KEY `FK_FoneRestaurante_3` (`idRestaurante`),
  CONSTRAINT `FK_FoneRestaurante_0` FOREIGN KEY (`idDDI`) REFERENCES `ddi` (`idDDI`),
  CONSTRAINT `FK_FoneRestaurante_1` FOREIGN KEY (`idDDD`) REFERENCES `ddd` (`idDDD`),
  CONSTRAINT `FK_FoneRestaurante_2` FOREIGN KEY (`idTipoFone`) REFERENCES `tipofone` (`idTipoFone`),
  CONSTRAINT `FK_FoneRestaurante_3` FOREIGN KEY (`idRestaurante`) REFERENCES `restaurante` (`idRestaurante`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fonerestaurante`
--

LOCK TABLES `fonerestaurante` WRITE;
/*!40000 ALTER TABLE `fonerestaurante` DISABLE KEYS */;
/*!40000 ALTER TABLE `fonerestaurante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemcompra`
--

DROP TABLE IF EXISTS `itemcompra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `itemcompra` (
  `idNotaCompra` int(11) NOT NULL,
  `idProduto` int(11) NOT NULL,
  `quantidade` int(11) DEFAULT NULL,
  `precoUnitario` float DEFAULT NULL,
  PRIMARY KEY (`idNotaCompra`,`idProduto`),
  KEY `FK_ItemCompra_1` (`idProduto`),
  CONSTRAINT `FK_ItemCompra_0` FOREIGN KEY (`idNotaCompra`) REFERENCES `notacompra` (`idNotaCompra`),
  CONSTRAINT `FK_ItemCompra_1` FOREIGN KEY (`idProduto`) REFERENCES `produto` (`idProduto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemcompra`
--

LOCK TABLES `itemcompra` WRITE;
/*!40000 ALTER TABLE `itemcompra` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemcompra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemnota`
--

DROP TABLE IF EXISTS `itemnota`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `itemnota` (
  `idProduto` int(11) NOT NULL,
  `idNotaVenda` int(11) NOT NULL,
  `quantidade` int(11) DEFAULT NULL,
  `precoUnitario` float DEFAULT NULL,
  PRIMARY KEY (`idProduto`,`idNotaVenda`),
  KEY `FK_ItemNota_1` (`idNotaVenda`),
  CONSTRAINT `FK_ItemNota_0` FOREIGN KEY (`idProduto`) REFERENCES `produto` (`idProduto`),
  CONSTRAINT `FK_ItemNota_1` FOREIGN KEY (`idNotaVenda`) REFERENCES `notavenda` (`idNotaVenda`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemnota`
--

LOCK TABLES `itemnota` WRITE;
/*!40000 ALTER TABLE `itemnota` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemnota` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logradouro`
--

DROP TABLE IF EXISTS `logradouro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `logradouro` (
  `idLogradouro` int(11) NOT NULL,
  `nomeLogradouro` varchar(100) NOT NULL,
  `idTipoLogradouro` int(11) DEFAULT NULL,
  PRIMARY KEY (`idLogradouro`),
  KEY `FK_Logradouro_0` (`idTipoLogradouro`),
  CONSTRAINT `FK_Logradouro_0` FOREIGN KEY (`idTipoLogradouro`) REFERENCES `tipologradouro` (`idTipoLogradouro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logradouro`
--

LOCK TABLES `logradouro` WRITE;
/*!40000 ALTER TABLE `logradouro` DISABLE KEYS */;
INSERT INTO `logradouro` VALUES (1,'Vitorino',1),(2,'Brasil',2),(3,'Costa e Silva',2);
/*!40000 ALTER TABLE `logradouro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notacompra`
--

DROP TABLE IF EXISTS `notacompra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notacompra` (
  `idNotaCompra` int(11) NOT NULL,
  `dataNota` date DEFAULT NULL,
  `totalNota` float DEFAULT NULL,
  `descontoTotal` float DEFAULT NULL,
  `valorLiquido` float DEFAULT NULL,
  `idRestaurante` int(11) DEFAULT NULL,
  PRIMARY KEY (`idNotaCompra`),
  KEY `FK_NotaCompra_0` (`idRestaurante`),
  CONSTRAINT `FK_NotaCompra_0` FOREIGN KEY (`idRestaurante`) REFERENCES `restaurante` (`idRestaurante`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notacompra`
--

LOCK TABLES `notacompra` WRITE;
/*!40000 ALTER TABLE `notacompra` DISABLE KEYS */;
/*!40000 ALTER TABLE `notacompra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notavenda`
--

DROP TABLE IF EXISTS `notavenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notavenda` (
  `idNotaVenda` int(11) NOT NULL,
  `dataNota` date DEFAULT NULL,
  `totalNota` float DEFAULT NULL,
  `descontoTotal` float DEFAULT NULL,
  `valorLiquido` float DEFAULT NULL,
  `idCliente` int(11) DEFAULT NULL,
  `idEntregador` int(11) DEFAULT NULL,
  PRIMARY KEY (`idNotaVenda`),
  KEY `FK_NotaVenda_0` (`idCliente`),
  KEY `FK_NotaVenda_1` (`idEntregador`),
  CONSTRAINT `FK_NotaVenda_0` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`),
  CONSTRAINT `FK_NotaVenda_1` FOREIGN KEY (`idEntregador`) REFERENCES `entregador` (`idEntregador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notavenda`
--

LOCK TABLES `notavenda` WRITE;
/*!40000 ALTER TABLE `notavenda` DISABLE KEYS */;
/*!40000 ALTER TABLE `notavenda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orgaoexpeditor`
--

DROP TABLE IF EXISTS `orgaoexpeditor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orgaoexpeditor` (
  `idOrgaoExpeditor` int(11) NOT NULL,
  `nomeOrgaoExpeditor` varchar(100) NOT NULL,
  `idUF` int(11) DEFAULT NULL,
  PRIMARY KEY (`idOrgaoExpeditor`),
  KEY `FK_OrgaoExpeditor_0` (`idUF`),
  CONSTRAINT `FK_OrgaoExpeditor_0` FOREIGN KEY (`idUF`) REFERENCES `uf` (`idUF`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orgaoexpeditor`
--

LOCK TABLES `orgaoexpeditor` WRITE;
/*!40000 ALTER TABLE `orgaoexpeditor` DISABLE KEYS */;
INSERT INTO `orgaoexpeditor` VALUES (1,'SSP',1);
/*!40000 ALTER TABLE `orgaoexpeditor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pais`
--

DROP TABLE IF EXISTS `pais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pais` (
  `idPais` int(11) NOT NULL,
  `nomePais` varchar(100) NOT NULL,
  PRIMARY KEY (`idPais`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pais`
--

LOCK TABLES `pais` WRITE;
/*!40000 ALTER TABLE `pais` DISABLE KEYS */;
INSERT INTO `pais` VALUES (1,'Brasil');
/*!40000 ALTER TABLE `pais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produto`
--

DROP TABLE IF EXISTS `produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `produto` (
  `idProduto` int(11) NOT NULL,
  `precoCustoAtual` float DEFAULT NULL,
  `quantidadeEstoque` int(11) DEFAULT NULL,
  `nomeProduto` varchar(100) DEFAULT NULL,
  `codBarras` varchar(100) DEFAULT NULL,
  `precoVendaAtual` float DEFAULT NULL,
  `idRestaurante` int(11) DEFAULT NULL,
  PRIMARY KEY (`idProduto`),
  KEY `FK_Produto_0` (`idRestaurante`),
  CONSTRAINT `FK_Produto_0` FOREIGN KEY (`idRestaurante`) REFERENCES `restaurante` (`idRestaurante`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produto`
--

LOCK TABLES `produto` WRITE;
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
INSERT INTO `produto` VALUES (1,1.5,100,'Palha Italiana',NULL,5,1),(2,0.2,100,'Brigadeiro',NULL,1.5,1),(3,5,100,'Fatia de Bolo',NULL,12,1),(4,20,100,'Frango Teriyaki',NULL,48.6,2),(5,13.5,100,'Frango Grelhado',NULL,43.2,2),(6,20,100,'Carne Supreme',NULL,48.6,2),(7,8.35,100,'Box Mineiro',NULL,25.9,3),(8,8.4,100,'Box Carreteiro',NULL,26.9,3),(9,8.2,100,'Box Espaguete',NULL,24.9,3),(10,3.65,100,'Cheese Salad',NULL,15.9,4),(11,4,100,'Chicken Bacon',NULL,18.9,4),(12,4,100,'Cheese Egg',NULL,18.9,4),(13,5.55,100,'Whopper',NULL,27.9,5),(14,3.75,100,'Combo Chicken Jr',NULL,19.9,5),(15,6,100,'Vegetariano',NULL,29.9,5),(16,0.3,100,'BomBom Zero Acucar',NULL,2.7,6),(17,0.35,100,'Tablete ao Leite',NULL,3.9,6),(18,0.5,100,'Alfajor ao Leite',NULL,7.9,6),(19,11.45,100,'Brasileirinho Bowl Ovo',NULL,25,7),(20,12,100,'Milanesa Vegetariana',NULL,33,7),(21,11.45,100,'Croc Vegetariano',NULL,25,7),(22,8.34,100,'Big Mac',NULL,25.9,8),(23,8.3,100,'Cheddar McMelt',NULL,25.5,8),(24,7.55,100,'McChicken',NULL,24.9,8),(25,23.3,100,'Hamburger Bovino',NULL,51.1,9),(26,25,100,'Hamburger de Cordeiro',NULL,59.8,9),(27,23.3,100,'Madero Vegetariano',NULL,51.1,9);
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurante`
--

DROP TABLE IF EXISTS `restaurante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `restaurante` (
  `idRestaurante` int(11) NOT NULL,
  `nomeAbreviadoRestaurante` varchar(100) DEFAULT NULL,
  `nomeCompletoRestaurante` varchar(100) DEFAULT NULL,
  `cnpj` varchar(100) DEFAULT NULL,
  `numero` int(11) DEFAULT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `idEndereco` int(11) DEFAULT NULL,
  PRIMARY KEY (`idRestaurante`),
  KEY `FK_Restaurante_0` (`idEndereco`),
  CONSTRAINT `FK_Restaurante_0` FOREIGN KEY (`idEndereco`) REFERENCES `endereco` (`idEndereco`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurante`
--

LOCK TABLES `restaurante` WRITE;
/*!40000 ALTER TABLE `restaurante` DISABLE KEYS */;
INSERT INTO `restaurante` VALUES (1,'Doce Amor','Doce Amor doceria artesanal','41953333000112',457,'Na esquina',1),(2,'Subway','Subway S.A.','37523630000150',185,'Cataratas JL Shopping',2),(3,'Ponto Mineiro','Ponto Mineiro S.A.','97551755000116',185,'Cataratas JL Shopping',2),(4,'Bobs','Bobs S.A.','68745266000105',185,'Cataratas JL Shopping',2),(5,'Burguer King','Burguer King S.A.','16254581000128',185,'Cataratas JL Shopping',2),(6,'Cacau Show','Cacau Show S.A.','01606530000140',185,'Cataratas JL Shopping',2),(7,'Giraffas','Giraffas S.A.','72094112000160',185,'Cataratas JL Shopping',2),(8,'McDonalds','McDonalds S.A.','48687468000122',185,'Cataratas JL Shopping',2),(9,'Madero','Madero S.A.','24410759000100',185,'Cataratas JL Shopping',2);
/*!40000 ALTER TABLE `restaurante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sexo`
--

DROP TABLE IF EXISTS `sexo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sexo` (
  `idSexo` int(11) NOT NULL,
  `descricaoSexo` varchar(100) NOT NULL,
  `siglaSexo` varchar(100) NOT NULL,
  PRIMARY KEY (`idSexo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sexo`
--

LOCK TABLES `sexo` WRITE;
/*!40000 ALTER TABLE `sexo` DISABLE KEYS */;
INSERT INTO `sexo` VALUES (1,'Masculino','M'),(2,'Feminino','F');
/*!40000 ALTER TABLE `sexo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipofone`
--

DROP TABLE IF EXISTS `tipofone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipofone` (
  `idTipoFone` int(11) NOT NULL,
  `siglaTipoFone` varchar(100) NOT NULL,
  `descricaoTipoFone` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idTipoFone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipofone`
--

LOCK TABLES `tipofone` WRITE;
/*!40000 ALTER TABLE `tipofone` DISABLE KEYS */;
INSERT INTO `tipofone` VALUES (1,'Cel','Celular');
/*!40000 ALTER TABLE `tipofone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipologradouro`
--

DROP TABLE IF EXISTS `tipologradouro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipologradouro` (
  `idTipoLogradouro` int(11) NOT NULL,
  `siglaTipo` varchar(100) DEFAULT NULL,
  `nomeTIpo` varchar(100) NOT NULL,
  PRIMARY KEY (`idTipoLogradouro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipologradouro`
--

LOCK TABLES `tipologradouro` WRITE;
/*!40000 ALTER TABLE `tipologradouro` DISABLE KEYS */;
INSERT INTO `tipologradouro` VALUES (1,'RUA','rua'),(2,'AV','avenida');
/*!40000 ALTER TABLE `tipologradouro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uf`
--

DROP TABLE IF EXISTS `uf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `uf` (
  `idUF` int(11) NOT NULL,
  `siglaUF` varchar(10) DEFAULT NULL,
  `nomeUF` varchar(100) NOT NULL,
  `idPais` int(11) DEFAULT NULL,
  PRIMARY KEY (`idUF`),
  KEY `FK_UF_0` (`idPais`),
  CONSTRAINT `FK_UF_0` FOREIGN KEY (`idPais`) REFERENCES `pais` (`idPais`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uf`
--

LOCK TABLES `uf` WRITE;
/*!40000 ALTER TABLE `uf` DISABLE KEYS */;
INSERT INTO `uf` VALUES (1,'PR','Parana',1);
/*!40000 ALTER TABLE `uf` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-19 12:37:00
