

import java.util.logging.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ComidaEmCasa
 */
@WebServlet("/ComidaEmCasa")
public class ComidaEmCasa extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final Logger LOGGER = Logger.getLogger(ComidaEmCasa.class.getName());
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ComidaEmCasa() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		
		try {
			response.getWriter().append("Served at: ").append(request.getContextPath());
		}catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.log(null,"context",e);
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		
		try {
			doGet(request, response);
		}catch (Exception e) {
			// TODO Auto-generated catch block
			LOGGER.log(null,"context",e);
		}
	}

}
